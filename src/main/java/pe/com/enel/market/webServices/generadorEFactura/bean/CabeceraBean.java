package pe.com.enel.market.webServices.generadorEFactura.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="rep_digital_cabecera")
public class CabeceraBean {

	@Id
	@Column(name="id_digital_cabecera")
	private Long id;
	
	@Column(name="id_documento")
	private Long idDocumento;
	
	@Column(name="estado_envio_sf")
	private String estadoEnvioSF;
	
	@Column(name="contador")
	private Integer contador;
	
	@Column(name="fecha_primer_envio")
	private Date fechaPrimerEnvio;

	@Column(name="fecha_ultimo_envio")
	private Date fechaUltimoEnvio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getEstadoEnvioSF() {
		return estadoEnvioSF;
	}

	public void setEstadoEnvioSF(String estadoEnvioSF) {
		this.estadoEnvioSF = estadoEnvioSF;
	}

	public Integer getContador() {
		return contador;
	}

	public void setContador(Integer contador) {
		this.contador = contador;
	}

	public Date getFechaPrimerEnvio() {
		return fechaPrimerEnvio;
	}

	public void setFechaPrimerEnvio(Date fechaPrimerEnvio) {
		this.fechaPrimerEnvio = fechaPrimerEnvio;
	}

	public Date getFechaUltimoEnvio() {
		return fechaUltimoEnvio;
	}

	public void setFechaUltimoEnvio(Date fechaUltimoEnvio) {
		this.fechaUltimoEnvio = fechaUltimoEnvio;
	}
	
	
	
}
