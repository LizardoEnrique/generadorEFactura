package pe.com.enel.market.webServices.generadorEFactura.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="SF_RECIBO_CONSUMO")
public class ConsumoBean implements Serializable{	
	
	@Id
	@Column(name="id_consumo")
	@Expose
	private Long idConsumo;
	
	@Transient
	private FacturaBean factura;
	
	@Column(name="mes")
	@Expose
	private Long mes;
	
	@Column(name="consumo")
	@Expose
	private Long consumo;
	
	@Column(name="cons_01")
	@Expose
	private Long cons1;
	
	@Column(name="cons_02")
	@Expose
	private Long cons2;
	
	@Column(name="mes_c")
	@Expose
	private String  mesC;

	public Long getIdConsumo() {
		return idConsumo;
	}

	public void setIdConsumo(Long idConsumo) {
		this.idConsumo = idConsumo;
	}

	public FacturaBean getFactura() {
		return factura;
	}

	public void setFactura(FacturaBean factura) {
		this.factura = factura;
	}

	public Long getMes() {
		return mes;
	}

	public void setMes(Long mes) {
		this.mes = mes;
	}

	public Long getConsumo() {
		return consumo;
	}

	public void setConsumo(Long consumo) {
		this.consumo = consumo;
	}

	public Long getCons1() {
		return cons1;
	}

	public void setCons1(Long cons1) {
		this.cons1 = cons1;
	}

	public Long getCons2() {
		return cons2;
	}

	public void setCons2(Long cons2) {
		this.cons2 = cons2;
	}

	public String getMesC() {
		return mesC;
	}

	public void setMesC(String mesC) {
		this.mesC = mesC;
	}


}