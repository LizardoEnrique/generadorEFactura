package pe.com.enel.market.webServices.generadorEFactura.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.enel.market.webServices.generadorEFactura.bean.ParametrosBean;

public interface ParametrosRepository extends JpaRepository<ParametrosBean,Long>{

	public List<ParametrosBean> findAllByVigente(String vigente);
}
