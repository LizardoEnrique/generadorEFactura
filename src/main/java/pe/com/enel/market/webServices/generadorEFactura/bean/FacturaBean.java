package pe.com.enel.market.webServices.generadorEFactura.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="SF_RECIBO_DIGITAL")
public class FacturaBean implements Serializable {

	@Id
	@Column(name="id_digital_cabecera")
	@Expose
	private Long idCabecera;
	
	@Column(name="id_cuenta")
	@Expose
	private Integer idCuenta;
	
	@Column(name="nombre_archivo")
	@Expose
	private String nombreArchivo;
	
	@Column(name="nro_cuenta")
	@Expose
	private String nroCuenta;
	
	@Column(name="nombre_corto")
	@Expose
	private String nombreCorto;
	
	@Column(name="direccion_corto")
	@Expose
	private String direccionCorto;
	
	@Column(name="sistema_electrico")
	@Expose
	private String sistemaElectrico;
	
	@Column(name="nro_ruc")
	@Expose
	private String nroRuc;
	
	@Column(name="nro_recibo")
	@Expose
	private String nroRecibo;
	
	@Column(name="nro_pagina")
	@Expose
	private String nroPagina;
	
	@Column(name="alimentador")
	@Expose
	private String alimentador;
	
	@Column(name="tarifa")
	@Expose
	private String tarifa;
	
	@Column(name="pot_contratada")
	@Expose
	private String potContratada;
	
	@Column(name="medidor_fase")
	@Expose
	private String medidorFase;
	
	@Column(name="nro_medidor")
	@Expose
	private String nroMedidor;
	
	@Column(name="cant_hilos")
	@Expose
	private String cantHilos;
	
	@Column(name="tipo_cnx")
	@Expose
	private String tipoCnx;
	
	@Column(name="plg_tarifario")
	@Expose
	private String plgTarifario;
	
	@Column(name="mes_ano_factu")
	@Expose
	private String mesAnoFactu;
	
	@Column(name="fec_lec_act")
	@Expose
	private String fecLecAct;
	
	@Column(name="lec_act")
	@Expose
	private String lecAct;
	
	@Column(name="fec_lec_ant")
	@Expose
	private String fecLecAnt;
	
	@Column(name="lec_ant")
	@Expose
	private String lecAnt;
	
	@Column(name="factor")
	@Expose
	private String factor;
	
	@Column(name="det_cons")
	@Expose
	private String detCons;
	
	@Column(name="meses_cons")
	@Expose
	private String mesesCons;
	
	@Column(name="escala_y")
	@Expose
	private String escalaY;
	
	@Column(name="conceptos_txt")
	@Expose
	private String conceptosTxt;
	
	@Column(name="barra_consumos")
	@Expose
	private String barraConsumos;
	
	@Column(name="conceptos_imp")
	@Expose
	private String conceptosImp;
	
	@Column(name="fecha_emi")
	@Expose
	private String fechaEmi;
	
	@Column(name="ruta_fact")
	@Expose
	private String rutaFact;
	
	@Column(name="tension")
	@Expose
	private String tension;
	
	@Column(name="tipo_cnx_osi")
	@Expose
	private String tipoCnxOsi;
	
	@Column(name="msg_cliente")
	@Expose
	private String msgCliente;
	
	@Column(name="codigo_barra")
	@Expose
	private String codigoBarra;
	
	@Column(name="enc_cobr_txt")
	@Expose
	private String encCobrTxt;
	
	@Column(name="enc_cobr_imp")
	@Expose
	private String encCobrImp;
	
	@Column(name="enc_tot")
	@Expose
	private String encTot;
	
	@Column(name="tot_consumo")
	@Expose
	private String totConsumo;
	
	@Column(name="list_interrup")
	@Expose
	private String listInterrup;
	
	@Column(name="fse_beneficiario")
	@Expose
	private String fseBeneficiario;
	
	@Column(name="fse_dni")
	@Expose
	private String fseDni;
	
	@Column(name="fse_dep")
	@Expose
	private String fseDep;
	
	@Column(name="fse_valor")
	@Expose
	private String fseValor;
	
	@Column(name="fse_fecvenc")
	@Expose
	private String fseFecvenc;
	
	@Column(name="fse_ticket")
	@Expose
	private String fseTicket;
	
	@Column(name="fse_tipo")
	@Expose
	private String fseTipo;
	
	@Column(name="total_pagar")
	@Expose
	private String totalPagar;
	
	@Column(name="flag_carita")
	@Expose
	private String flagCarita;
	
	@Column(name="fecha_vcto")
	@Expose
	private String fechaVcto;
	
	@Column(name="fecha_corte")
	@Expose
	private String fechaCorte;
	
	@Column(name="penul_fact_mes")
	@Expose
	private String penulFactMes;
	
	@Column(name="penul_fact_imp")
	@Expose
	private String penulFactImp;
	
	@Column(name="ult_fact_mes")
	@Expose
	private String ultFactMes;
	
	@Column(name="ult_fact_imp")
	@Expose
	private String ultFactImp;
	
	@Column(name="nro_secuencia")
	@Expose
	private String nroSecuencia;
	
	@Column(name="distrito")
	@Expose
	private String distrito;
	
	@Column(name="saldo_favor_txt")
	@Expose
	private String saldoFavorTxt;
	
	@Column(name="saldo_favor_imp")
	@Expose
	private String saldoFavorImp;
	
	@Column(name="car_cta_txt")
	@Expose
	private String carCtaTxt;
	
	@Column(name="nom_propietario")
	@Expose
	private String nomPropietario;
	
	@Column(name="titular_ampliado")
	@Expose
	private String titularAmpliado;
	
	@Column(name="dir_tit_ampliado")
	@Expose
	private String dirTitAmpliado;
	
	
	@Column(name="fecha_proceso")
	@Expose
	private Date fechaProceso;
	
	@Column(name="sf_nro_cuenta")
	@Expose
	private String sfNroCuenta;
	
	@Column(name="sf_nombre")
	@Expose
	private String sfNombre;
	
	@Column(name="sf_tot_documento")
	@Expose
	private String sfTotDocumento;
	
	@Column(name="sf_fec_vencimiento")
	@Expose
	private Date sfFecVen;
	
	@Column(name="sf_nro_doc_identidad")
	@Expose
	private String sfNroDocIdentidad;
	
	@Column(name="correo")
	@Expose
	private String correo; 
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy="factura")
	@Expose
	private List<HojaBean> hojaFactura;

	@Transient
	private List<ConsumoBean> consumoFactura;

	public Long getIdCabecera() {
		return idCabecera;
	}

	public void setIdCabecera(Long idCabecera) {
		this.idCabecera = idCabecera;
	}

	public Integer getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}

	public String getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public String getDireccionCorto() {
		return direccionCorto;
	}

	public void setDireccionCorto(String direccionCorto) {
		this.direccionCorto = direccionCorto;
	}

	public String getSistemaElectrico() {
		return sistemaElectrico;
	}

	public void setSistemaElectrico(String sistemaElectrico) {
		this.sistemaElectrico = sistemaElectrico;
	}

	public String getNroRuc() {
		return nroRuc;
	}

	public void setNroRuc(String nroRuc) {
		this.nroRuc = nroRuc;
	}

	public String getNroRecibo() {
		return nroRecibo;
	}

	public void setNroRecibo(String nroRecibo) {
		this.nroRecibo = nroRecibo;
	}

	public String getNroPagina() {
		return nroPagina;
	}

	public void setNroPagina(String nroPagina) {
		this.nroPagina = nroPagina;
	}

	public String getAlimentador() {
		return alimentador;
	}

	public void setAlimentador(String alimentador) {
		this.alimentador = alimentador;
	}

	public String getTarifa() {
		return tarifa;
	}

	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	public String getPotContratada() {
		return potContratada;
	}

	public void setPotContratada(String potContratada) {
		this.potContratada = potContratada;
	}

	public String getMedidorFase() {
		return medidorFase;
	}

	public void setMedidorFase(String medidorFase) {
		this.medidorFase = medidorFase;
	}

	public String getNroMedidor() {
		return nroMedidor;
	}

	public void setNroMedidor(String nroMedidor) {
		this.nroMedidor = nroMedidor;
	}

	public String getCantHilos() {
		return cantHilos;
	}

	public void setCantHilos(String cantHilos) {
		this.cantHilos = cantHilos;
	}

	public String getTipoCnx() {
		return tipoCnx;
	}

	public void setTipoCnx(String tipoCnx) {
		this.tipoCnx = tipoCnx;
	}

	public String getPlgTarifario() {
		return plgTarifario;
	}

	public void setPlgTarifario(String plgTarifario) {
		this.plgTarifario = plgTarifario;
	}

	public String getMesAnoFactu() {
		return mesAnoFactu;
	}

	public void setMesAnoFactu(String mesAnoFactu) {
		this.mesAnoFactu = mesAnoFactu;
	}

	public String getFecLecAct() {
		return fecLecAct;
	}

	public void setFecLecAct(String fecLecAct) {
		this.fecLecAct = fecLecAct;
	}

	public String getLecAct() {
		return lecAct;
	}

	public void setLecAct(String lecAct) {
		this.lecAct = lecAct;
	}

	public String getFecLecAnt() {
		return fecLecAnt;
	}

	public void setFecLecAnt(String fecLecAnt) {
		this.fecLecAnt = fecLecAnt;
	}

	public String getLecAnt() {
		return lecAnt;
	}

	public void setLecAnt(String lecAnt) {
		this.lecAnt = lecAnt;
	}

	public String getFactor() {
		return factor;
	}

	public void setFactor(String factor) {
		this.factor = factor;
	}

	public String getDetCons() {
		return detCons;
	}

	public void setDetCons(String detCons) {
		this.detCons = detCons;
	}

	public String getMesesCons() {
		return mesesCons;
	}

	public void setMesesCons(String mesesCons) {
		this.mesesCons = mesesCons;
	}

	public String getEscalaY() {
		return escalaY;
	}

	public void setEscalaY(String escalaY) {
		this.escalaY = escalaY;
	}

	public String getConceptosTxt() {
		return conceptosTxt;
	}

	public void setConceptosTxt(String conceptosTxt) {
		this.conceptosTxt = conceptosTxt;
	}

	public String getBarraConsumos() {
		return barraConsumos;
	}

	public void setBarraConsumos(String barraConsumos) {
		this.barraConsumos = barraConsumos;
	}

	public String getConceptosImp() {
		return conceptosImp;
	}

	public void setConceptosImp(String conceptosImp) {
		this.conceptosImp = conceptosImp;
	}

	public String getFechaEmi() {
		return fechaEmi;
	}

	public void setFechaEmi(String fechaEmi) {
		this.fechaEmi = fechaEmi;
	}

	public String getRutaFact() {
		return rutaFact;
	}

	public void setRutaFact(String rutaFact) {
		this.rutaFact = rutaFact;
	}

	public String getTension() {
		return tension;
	}

	public void setTension(String tension) {
		this.tension = tension;
	}

	public String getTipoCnxOsi() {
		return tipoCnxOsi;
	}

	public void setTipoCnxOsi(String tipoCnxOsi) {
		this.tipoCnxOsi = tipoCnxOsi;
	}

	public String getMsgCliente() {
		return msgCliente;
	}

	public void setMsgCliente(String msgCliente) {
		this.msgCliente = msgCliente;
	}

	public String getCodigoBarra() {
		return codigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public String getEncCobrTxt() {
		return encCobrTxt;
	}

	public void setEncCobrTxt(String encCobrTxt) {
		this.encCobrTxt = encCobrTxt;
	}

	public String getEncCobrImp() {
		return encCobrImp;
	}

	public void setEncCobrImp(String encCobrImp) {
		this.encCobrImp = encCobrImp;
	}

	public String getEncTot() {
		return encTot;
	}

	public void setEncTot(String encTot) {
		this.encTot = encTot;
	}

	public String getTotConsumo() {
		return totConsumo;
	}

	public void setTotConsumo(String totConsumo) {
		this.totConsumo = totConsumo;
	}

	public String getListInterrup() {
		return listInterrup;
	}

	public void setListInterrup(String listInterrup) {
		this.listInterrup = listInterrup;
	}

	public String getFseBeneficiario() {
		return fseBeneficiario;
	}

	public void setFseBeneficiario(String fseBeneficiario) {
		this.fseBeneficiario = fseBeneficiario;
	}

	public String getFseDni() {
		return fseDni;
	}

	public void setFseDni(String fseDni) {
		this.fseDni = fseDni;
	}

	public String getFseDep() {
		return fseDep;
	}

	public void setFseDep(String fseDep) {
		this.fseDep = fseDep;
	}

	public String getFseValor() {
		return fseValor;
	}

	public void setFseValor(String fseValor) {
		this.fseValor = fseValor;
	}

	public String getFseFecvenc() {
		return fseFecvenc;
	}

	public void setFseFecvenc(String fseFecvenc) {
		this.fseFecvenc = fseFecvenc;
	}

	public String getFseTicket() {
		return fseTicket;
	}

	public void setFseTicket(String fseTicket) {
		this.fseTicket = fseTicket;
	}

	public String getFseTipo() {
		return fseTipo;
	}

	public void setFseTipo(String fseTipo) {
		this.fseTipo = fseTipo;
	}

	public String getTotalPagar() {
		return totalPagar;
	}

	public void setTotalPagar(String totalPagar) {
		this.totalPagar = totalPagar;
	}

	public String getFlagCarita() {
		return flagCarita;
	}

	public void setFlagCarita(String flagCarita) {
		this.flagCarita = flagCarita;
	}

	public String getFechaVcto() {
		return fechaVcto;
	}

	public void setFechaVcto(String fechaVcto) {
		this.fechaVcto = fechaVcto;
	}

	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public String getPenulFactMes() {
		return penulFactMes;
	}

	public void setPenulFactMes(String penulFactMes) {
		this.penulFactMes = penulFactMes;
	}

	public String getPenulFactImp() {
		return penulFactImp;
	}

	public void setPenulFactImp(String penulFactImp) {
		this.penulFactImp = penulFactImp;
	}

	public String getUltFactMes() {
		return ultFactMes;
	}

	public void setUltFactMes(String ultFactMes) {
		this.ultFactMes = ultFactMes;
	}

	public String getUltFactImp() {
		return ultFactImp;
	}

	public void setUltFactImp(String ultFactImp) {
		this.ultFactImp = ultFactImp;
	}

	public String getNroSecuencia() {
		return nroSecuencia;
	}

	public void setNroSecuencia(String nroSecuencia) {
		this.nroSecuencia = nroSecuencia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getSaldoFavorTxt() {
		return saldoFavorTxt;
	}

	public void setSaldoFavorTxt(String saldoFavorTxt) {
		this.saldoFavorTxt = saldoFavorTxt;
	}

	public String getSaldoFavorImp() {
		return saldoFavorImp;
	}

	public void setSaldoFavorImp(String saldoFavorImp) {
		this.saldoFavorImp = saldoFavorImp;
	}

	public String getCarCtaTxt() {
		return carCtaTxt;
	}

	public void setCarCtaTxt(String carCtaTxt) {
		this.carCtaTxt = carCtaTxt;
	}

	public String getNomPropietario() {
		return nomPropietario;
	}

	public void setNomPropietario(String nomPropietario) {
		this.nomPropietario = nomPropietario;
	}

	public String getTitularAmpliado() {
		return titularAmpliado;
	}

	public void setTitularAmpliado(String titularAmpliado) {
		this.titularAmpliado = titularAmpliado;
	}

	public String getDirTitAmpliado() {
		return dirTitAmpliado;
	}

	public void setDirTitAmpliado(String dirTitAmpliado) {
		this.dirTitAmpliado = dirTitAmpliado;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public String getSfNroCuenta() {
		return sfNroCuenta;
	}

	public void setSfNroCuenta(String sfNroCuenta) {
		this.sfNroCuenta = sfNroCuenta;
	}

	public String getSfNombre() {
		return sfNombre;
	}

	public void setSfNombre(String sfNombre) {
		this.sfNombre = sfNombre;
	}

	public String getSfTotDocumento() {
		return sfTotDocumento;
	}

	public void setSfTotDocumento(String sfTotDocumento) {
		this.sfTotDocumento = sfTotDocumento;
	}

	public Date getSfFecVen() {
		return sfFecVen;
	}

	public void setSfFecVen(Date sfFecVen) {
		this.sfFecVen = sfFecVen;
	}

	public String getSfNroDocIdentidad() {
		return sfNroDocIdentidad;
	}

	public void setSfNroDocIdentidad(String sfNroDocIdentidad) {
		this.sfNroDocIdentidad = sfNroDocIdentidad;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public List<HojaBean> getHojaFactura() {
		return hojaFactura;
	}

	public void setHojaFactura(List<HojaBean> hojaFactura) {
		this.hojaFactura = hojaFactura;
	}

	public List<ConsumoBean> getConsumoFactura() {
		return consumoFactura;
	}

	public void setConsumoFactura(List<ConsumoBean> consumoFactura) {
		this.consumoFactura = consumoFactura;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	
}
