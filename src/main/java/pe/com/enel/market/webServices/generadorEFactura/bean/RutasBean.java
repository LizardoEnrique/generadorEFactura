package pe.com.enel.market.webServices.generadorEFactura.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="com_parametros")
public class RutasBean {
	
	@Id
	@Column(name="id_parametro")
	private Long id;
	
	@Column(name="id_empresa")
	private Long idEmpresa;
	
	@Column(name="sistema")
	private String sistema;
	
	@Column(name="entidad")
	private String entidad;
	
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="valor_alf")
	private String valorAlf;
	
	@Column(name="activo")
	private String activo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getValorAlf() {
		return valorAlf;
	}

	public void setValorAlf(String valorAlf) {
		this.valorAlf = valorAlf;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}
	
	
	
	
	
	

}
