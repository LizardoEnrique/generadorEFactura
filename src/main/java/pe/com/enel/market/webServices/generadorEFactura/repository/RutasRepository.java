package pe.com.enel.market.webServices.generadorEFactura.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.enel.market.webServices.generadorEFactura.bean.RutasBean;

public interface RutasRepository extends JpaRepository<RutasBean, Long>	{

	public List<RutasBean> findAllBySistemaAndEntidadAndActivo(String sistema, String entidad, String activo);
}
