package pe.com.enel.market.webServices.generadorEFactura.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.enel.market.lib.generadorPDFeFactura.GeneradorPDF;
import pe.com.enel.market.webServices.generadorEFactura.bean.FacturaBean;
import pe.com.enel.market.webServices.generadorEFactura.bean.ParametrosBean;
import pe.com.enel.market.webServices.generadorEFactura.bean.RutasBean;
import pe.com.enel.market.webServices.generadorEFactura.bean.SalesForceBean;
import pe.com.enel.market.webServices.generadorEFactura.mapping.MapasUtil;
import pe.com.enel.market.webServices.generadorEFactura.repository.FacturaRepository;
import pe.com.enel.market.webServices.generadorEFactura.repository.ParametrosRepository;
import pe.com.enel.market.webServices.generadorEFactura.repository.RutasRepository;
import pe.com.enel.market.webServices.generadorEFactura.service.FacturaService;
import pe.com.enel.market.webServices.generadorEFactura.util.Constantes;
import pe.com.enel.market.webServices.generadorEFactura.util.PropertiesExternos;
import pe.com.enel.market.webServices.generadorEFactura.util.Util;

@Service
public class FacturaServiceImpl implements FacturaService {

	Logger LOOGER = Logger.getLogger(FacturaServiceImpl.class);
	@Autowired
	private FacturaRepository facturaRepo;
	@Autowired
	private ParametrosRepository parametrosRepo;
	@Autowired
	private RutasRepository rutasRepo;
	@Autowired
	PropertiesExternos propiedades;

	@Override
	public List<FacturaBean> findAll() {
		// TODO Auto-generated method stub
		return facturaRepo.findAll();
	}

	@Override
	public List<FacturaBean> generarEfacturas(String rutaImg, String rutaPlantillas, String rutaPDF) {
		LOOGER.info("Generando facturas");
		List<FacturaBean> data = this.findAll();
		List<FacturaBean> listResult = new ArrayList<FacturaBean>();
		List<ParametrosBean> param = parametrosRepo.findAllByVigente("S");
		Map<Long,String> mapParam = new HashMap<Long,String>();
		for (ParametrosBean p : param){
			mapParam.put(p.getIdParm(), p.getNombrePlantilla());
		}
		for (FacturaBean fact : data){
			List <FacturaBean> datos = new ArrayList<FacturaBean>();
    		datos.add(fact);
			String s = Util.serialize(datos);
			GeneradorPDF generadorPDF = new GeneradorPDF();
			int result = generadorPDF.generarPDF(s, mapParam, rutaImg, rutaPlantillas, rutaPDF);
			if (result > 0){
				listResult.add(fact);
			}
		}
		LOOGER.info("Fin Generando facturas");
		return listResult;
		
	}

	@Override
	public List<RutasBean> getRutas() {
		LOOGER.info("Obteniendo las rutas");
		return rutasRepo.findAllBySistemaAndEntidadAndActivo("EFACTURA", "PATHS", "S");
	}

	@Override
	public String procesarEFactura() {
		List<RutasBean> rutas = getRutas();
		String rutaImg = "d:\\test\\";
		String rutaPlantillas = "d:\\test\\";
		String rutaPDF = "d:\\test\\";
		String rutaCSV = "d:\\test\\csv\\";
		String rutaServ = "http://";
		/*for (RutasBean r : rutas){
			if (r.getCodigo().equals("PATH_PLA")) rutaPlantillas = r.getValorAlf();
			if (r.getCodigo().equals("PATH_IMA")) rutaImg = r.getValorAlf();
			if (r.getCodigo().equals("PATH_PDF")) rutaPDF = r.getValorAlf();
			if (r.getCodigo().equals("PATH_OUT")) rutaCSV = r.getValorAlf();
			if (r.getCodigo().equals("DOC_DIG")) rutaServ = r.getValorAlf();
		}*/
		List<FacturaBean> list = generarEfacturas(rutaImg, rutaPlantillas, rutaPDF);
		
		List<SalesForceBean> listSF = MapasUtil.mapearFacturaASalesForce(list, rutaServ);
		String respuesta= Util.GenerarCSV(listSF, propiedades.getSalesForceColums().split(Constantes.COMA), rutaCSV,Constantes.DELIMITADOR);
		//Actualizar registros OK	
		actualizaRegistros(list);
		return "Registros Procesados: " + list.size()+ " - " +  (respuesta.equals(Constantes.CERO_STRING)?"EXITO":"ERROR");
	}

	public int actualizaRegistros(List<FacturaBean> list) {
		for (FacturaBean fact : list){
			this.actualizaRegistro(fact);
		}
		return 0;
	}
	
	
	public int actualizaRegistro(FacturaBean fact){
		return this.facturaRepo.updateEnvioSF("S",fact.getIdCabecera());
	}

}
