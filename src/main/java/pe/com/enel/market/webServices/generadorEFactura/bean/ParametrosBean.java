package pe.com.enel.market.webServices.generadorEFactura.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="REP_DIGITAL_PARAM")
public class ParametrosBean {

	@Id
	@Column(name="id_digital_param")
	private Long idParm;
	
	@Column(name="cod_param")
	private String codParm;
	
	@Column(name="nombre_plantilla")
	private String nombrePlantilla;

	@Column(name="nombre_imagen")
	private String nombreImagen;
	
	@Column(name="cod_interno")
	private String codInterno;
	
	@Column(name="vigente")
	private String vigente;
	
	@Column(name="tiene_rango")
	private String tieneRango;
	
	@Column(name="rango_inicio")
	private Integer rangoInicio;
	
	@Column(name="rango_fin")
	private Integer rangoFin;
	
	@Column(name="orden")
	private Integer orden;

	public Long getIdParm() {
		return idParm;
	}

	public void setIdParm(Long idParm) {
		this.idParm = idParm;
	}

	public String getCodParm() {
		return codParm;
	}

	public void setCodParm(String codParm) {
		this.codParm = codParm;
	}

	public String getNombrePlantilla() {
		return nombrePlantilla;
	}

	public void setNombrePlantilla(String nombrePlantilla) {
		this.nombrePlantilla = nombrePlantilla;
	}

	public String getNombreImagen() {
		return nombreImagen;
	}

	public void setNombreImagen(String nombreImagen) {
		this.nombreImagen = nombreImagen;
	}

	public String getCodInterno() {
		return codInterno;
	}

	public void setCodInterno(String codInterno) {
		this.codInterno = codInterno;
	}

	public String getVigente() {
		return vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

	public String getTieneRango() {
		return tieneRango;
	}

	public void setTieneRango(String tieneRango) {
		this.tieneRango = tieneRango;
	}

	public Integer getRangoInicio() {
		return rangoInicio;
	}

	public void setIrangoInicio(Integer irangoInicio) {
		this.rangoInicio = irangoInicio;
	}

	public Integer getRangoFin() {
		return rangoFin;
	}

	public void setRangoFin(Integer rangoFin) {
		this.rangoFin = rangoFin;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}
}
