package pe.com.enel.market.webServices.generadorEFactura.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="REP_DIGITAL_DETALLE")
public class HojaBean implements Serializable{
	
	@Id
	@Column(name="id_digital_detalle")
	@Expose
	private Long idDetalle;
	
	@ManyToOne
	@JoinColumn(name="id_digital_cabecera")
	private FacturaBean factura;
	
	@Column(name="id_digital_param")
	@Expose
	private Long idParam;
	
	@Column(name="nombre_imagen")
	@Expose
	private String nomImagen;

	@Column(name="rango_inicio")
	@Expose
	private Integer rangoInicio;
	
	@Column(name="rango_fin")
	@Expose
	private Integer rangoFin;
	
	@Column(name="orden")
	@Expose
	private int orden;

	public Long getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}

	public FacturaBean getFactura() {
		return factura;
	}

	public void setFactura(FacturaBean factura) {
		this.factura = factura;
	}

	public Long getIdParam() {
		return idParam;
	}

	public void setIdParam(Long idParam) {
		this.idParam = idParam;
	}

	public String getNomImagen() {
		return nomImagen;
	}

	public void setNomImagen(String nomImagen) {
		this.nomImagen = nomImagen;
	}

	public Integer getRangoInicio() {
		return rangoInicio;
	}

	public void setRangoInicio(Integer rangoInicio) {
		this.rangoInicio = rangoInicio;
	}

	public Integer getRangoFin() {
		return rangoFin;
	}

	public void setRangoFin(Integer rangoFin) {
		this.rangoFin = rangoFin;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}
	

}
