package pe.com.enel.market.webServices.generadorEFactura.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import pe.com.enel.market.webServices.generadorEFactura.bean.FacturaBean;

public interface FacturaRepository extends JpaRepository<FacturaBean, Long>{
	
	public List<FacturaBean> findAll();
	
	@Modifying
	@Query("update CabeceraBean u set u.estadoEnvioSF = ?1, u.contador = u.contador +1, u.fechaPrimerEnvio= (case when u.fechaPrimerEnvio=null then sysdate else u.fechaPrimerEnvio end), u.fechaUltimoEnvio = sysdate where u.id = ?2")
	@Transactional
	public int updateEnvioSF(String envio, Long id);


}
