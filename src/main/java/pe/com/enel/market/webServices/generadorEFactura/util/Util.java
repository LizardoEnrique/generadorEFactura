package pe.com.enel.market.webServices.generadorEFactura.util;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.csvreader.CsvWriter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pe.com.enel.market.webServices.generadorEFactura.bean.SalesForceBean;

public class Util {

	public static String ConvertirStringADecimales(String numero, int cantDecimales){
		Double numeroConvertido=0.0;
		String cantidadDecimalesString="";
		for(int i=0; i< cantDecimales; i++){
			cantidadDecimalesString+=Constantes.CERO_STRING;
		}
		try {
			numeroConvertido = Double.parseDouble(numero);
			NumberFormat formatter = new DecimalFormat("#0."+cantidadDecimalesString);
			return formatter.format(numeroConvertido);
			//formatter.
		} catch (Exception e) {
			e.printStackTrace();
			return Constantes.CERO_STRING_DOUBLE;
		}
		
	}
	
	public static String DateToStringFormato(Date d, String formato){
		DateFormat df = new SimpleDateFormat(formato);
		try {
			return df.format(d);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return Constantes.VACIO;
	}
	
	public static String GenerarCSV(List<SalesForceBean> s, String[] columnas, String rutaCSV, char delimitador){
		try {
			String outputFile = rutaCSV + "atcl_factura"+ Util.DateToStringFormato(modificarFecha(new Date(),GregorianCalendar.DAY_OF_MONTH,-1), Constantes.FORMATO_FECHA_RUTA) +".csv";
			//.replace(Constantes.CARACTER_REEMPLAZO, Util.DateToStringFormato(new Date(), Constantes.FORMATO_FECHA_RUTA))
			//{0}.csv
	        boolean alreadyExists = new File(outputFile).exists();
	        File ArchivoCSV;
	        if(alreadyExists){
	            ArchivoCSV = new File(outputFile);
	            ArchivoCSV.delete();
	        }/*else{
	        	ArchivoCSV = new File(outputFile);
	        	ArchivoCSV.createNewFile();
	        }*/
	        CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), delimitador);
	        
	        if(columnas.length>0){
	        	//Generando Cabeceras
				for(String c:columnas){
					csvOutput.write(c);
				}
				csvOutput.endRecord();
				
				//Datos
				for(SalesForceBean sf:s){
					csvOutput.write(sf.getNroSuministro());
					csvOutput.write(sf.getNombreTitular());
					csvOutput.write(sf.getTotalFacturado());
					csvOutput.write(sf.getFechaVencimiento());
					csvOutput.write(sf.getNroDocTitular());
					csvOutput.write(sf.getIdPropietario());
					csvOutput.write(sf.getCorreo());
					csvOutput.write(sf.getCorreo2());
					csvOutput.write(sf.getRuta());
					csvOutput.write(sf.getRutaPagar());
					csvOutput.write(sf.getUnsuscribe());
					csvOutput.write(sf.getCompany());
					csvOutput.endRecord();
				}
				csvOutput.close();
				
	        }
	        return Constantes.CERO_STRING;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Constantes.UNO_STRING;
	}
	
	public static String serialize(Object obj) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(obj);
	}
	
	public static Date modificarFecha(Date dia,int parteFecha, int cantidad){
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(dia);
		gc.add(parteFecha, cantidad);
		return gc.getTime();
	}
	
}
