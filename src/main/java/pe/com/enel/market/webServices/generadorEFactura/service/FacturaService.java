package pe.com.enel.market.webServices.generadorEFactura.service;

import java.util.List;


import pe.com.enel.market.webServices.generadorEFactura.bean.FacturaBean;
import pe.com.enel.market.webServices.generadorEFactura.bean.RutasBean;

public interface FacturaService {

	List<FacturaBean> findAll();

	public List<FacturaBean> generarEfacturas(String rutaImg, String rutaPlantillas, String rutaPDF);

	List<RutasBean> getRutas();

	String procesarEFactura();

}
