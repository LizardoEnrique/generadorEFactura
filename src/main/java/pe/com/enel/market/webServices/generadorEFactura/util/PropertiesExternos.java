package pe.com.enel.market.webServices.generadorEFactura.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
//@PropertySource("file:C:/test/impresionboleta.properties")
public class PropertiesExternos {
	
	@Value("${sales.force.colums}")
	private String salesForceColums;
	
	public String getSalesForceColums() {
		return salesForceColums;
	}

	public void setSalesForceColums(String salesForceColums) {
		this.salesForceColums = salesForceColums;
	}

}
