package pe.com.enel.market.webServices.generadorEFactura.mapping;

import java.util.ArrayList;
import java.util.List;

import pe.com.enel.market.webServices.generadorEFactura.bean.FacturaBean;
import pe.com.enel.market.webServices.generadorEFactura.bean.SalesForceBean;
import pe.com.enel.market.webServices.generadorEFactura.util.Constantes;
import pe.com.enel.market.webServices.generadorEFactura.util.Util;

public class MapasUtil {

	public static List<SalesForceBean> mapearFacturaASalesForce(List<FacturaBean> listaFactura, String rutaServ){
		List<SalesForceBean> listSF = new ArrayList<>();
		for(FacturaBean b:listaFactura){
			SalesForceBean sf = new SalesForceBean();
			sf.setNroSuministro(b.getSfNroCuenta());
			sf.setNombreTitular(b.getSfNombre());
			sf.setTotalFacturado(Util.ConvertirStringADecimales(b.getSfTotDocumento(), Constantes.DOS).replace(Constantes.COMA,Constantes.PUNTO));
			sf.setFechaVencimiento(Util.DateToStringFormato(b.getSfFecVen(), Constantes.FORMATO_FECHA_SF));
			sf.setNroDocTitular(b.getSfNroDocIdentidad());
			sf.setIdPropietario(Constantes.TITULAR_CUENTA);
			sf.setCorreo(b.getCorreo());
			sf.setCorreo2(Constantes.VACIO);
			sf.setRuta(rutaServ+""+b.getNombreArchivo());
			sf.setRutaPagar(Constantes.VACIO);
			sf.setUnsuscribe(Constantes.VACIO);
			sf.setCompany(Constantes.ENEL_DISTRIBUCION_PERU);
			listSF.add(sf);
		}
		
		return listSF;
	
	}
}
