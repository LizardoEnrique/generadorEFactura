package pe.com.enel.market.webServices.generadorEFactura.util;

public class Constantes {
	public static  String CERO_STRING="0";
	public static String PUNTO = ".";
	public static  String CERO_STRING_DOUBLE="0.0";
	public static  Integer DOS=2;
	public static String FORMATO_FECHA_SF="yyyy-MM-dd";
	public static String TITULAR_CUENTA="A";
	public static String VACIO="";
	public static String ENEL_DISTRIBUCION_PERU="7";
	public static String UNO_STRING="1";
	public static String COMA=",";
	public static String FORMATO_FECHA_RUTA="yyyyMMdd";
	public static String CARACTER_REEMPLAZO="{0}";
	public static char DELIMITADOR=';';
}
