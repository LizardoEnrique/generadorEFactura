package pe.com.enel.market.webServices.generadorEFactura;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import pe.com.enel.market.webServices.generadorEFactura.service.FacturaService;
import pe.com.enel.market.webServices.generadorEFactura.service.impl.FacturaServiceImpl;

/**
 * Hello world!
 *
 */

@Configuration
@EnableAutoConfiguration
@ComponentScan("pe.com.enel.market.webServices.generadorEFactura")
public class App implements CommandLineRunner
{
	
	@Autowired
	private FacturaService facturaService;
	
    public static void main( String[] args )
    {
    	//ApplicationContext context = SpringApplication.run(App.class, args);
    	SpringApplication app = new SpringApplication(App.class);
    	app.setBannerMode(Banner.Mode.OFF);
    	app.run(args);
    }
    
    @Override
    public void run(String... args){
    	String respuesta = facturaService.procesarEFactura();
    	System.out.println(respuesta);
    }
}
