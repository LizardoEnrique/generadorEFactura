package pe.com.enel.market.webServices.generadorEFactura.bean;

public class SalesForceBean {

	private String nroSuministro;
	private String nombreTitular;
	private String totalFacturado;
	private String fechaVencimiento;
	private String nroDocTitular;
	private String idPropietario;
	private String correo;
	private String correo2;
	private String ruta;
	private String rutaPagar;
	private String unsuscribe;
	private String company;
	
	
	public String getNroSuministro() {
		return nroSuministro;
	}
	public void setNroSuministro(String nroSuministro) {
		this.nroSuministro = nroSuministro;
	}
	public String getNombreTitular() {
		return nombreTitular;
	}
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	public String getTotalFacturado() {
		return totalFacturado;
	}
	public void setTotalFacturado(String totalFacturado) {
		this.totalFacturado = totalFacturado;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getNroDocTitular() {
		return nroDocTitular;
	}
	public void setNroDocTitular(String nroDocTitular) {
		this.nroDocTitular = nroDocTitular;
	}
	public String getIdPropietario() {
		return idPropietario;
	}
	public void setIdPropietario(String idPropietario) {
		this.idPropietario = idPropietario;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCorreo2() {
		return correo2;
	}
	public void setCorreo2(String correo2) {
		this.correo2 = correo2;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getRutaPagar() {
		return rutaPagar;
	}
	public void setRutaPagar(String rutaPagar) {
		this.rutaPagar = rutaPagar;
	}
	public String getUnsuscribe() {
		return unsuscribe;
	}
	public void setUnsuscribe(String unsuscribe) {
		this.unsuscribe = unsuscribe;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	
	
}
